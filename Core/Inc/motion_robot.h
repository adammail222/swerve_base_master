/*
 * motion_robot.h
 *
 *  Created on: Jan 10, 2023
 *      Author: adamm
 */

#ifndef INC_MOTION_ROBOT_H_
#define INC_MOTION_ROBOT_H_

void global_speed(float localSpeedOut[3], float globalSpeedOut[3]);
void motion_pure_pursuit(float target_x, float target_y, float target_z, float v_x, float v_y, float v_z, float globalSpeedOut_[3]);
void motion_jalan_x(float target_x, float target_y, float target_w, float v_x, float v_y, float v_w, float globalSpeedOut_[3]);




#endif /* INC_MOTION_ROBOT_H_ */
