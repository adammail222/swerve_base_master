/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "motion_robot.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* Variable For Odometry Rotary Encoder */
int16_t velRotary[2];
int16_t intRotary[2], prevIntRotary[2];
int16_t velLocal[2], positionLocal[2];
int16_t velGlobal[2];
float positionGlobal[3];
float globalSpeedOut[3], outputSpeedOut[3], localJoystickSpeedOut[3];
float kecepatanX, kecepatanY, kecepatanW;
float degree, degreeFix, filterDegree;

/* Variable For Send Data Master Slave */
uint8_t receiveSlave[31];
uint8_t sendSlave[31] = {'i','t','s'};

/* Variabel Joystick */
uint8_t terimaJoystick[9];
int16_t axisJoystick[4];
uint16_t buttonsJoystick, prevButtonsJoystick, btnMasuk;
uint8_t btnX, btnKotak, btnSegitiga, btnBulat, btnR1, btnR2, btnR3, btnL1, btnL2, btnL3, btnSelect, btnStart, btnAtas, btnKanan, btnBawah, btnKiri;

/* Variable For Send Data Master Arduino (Gyro) */
uint8_t receiveGyro[7];
uint8_t statReceiveGyro;
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart3_rx;

int test, test1, test2;
int countStick;

/* Mode For Auto Or Manual */
uint8_t stat_robot; // 0 = Manual || 1 = Auto




/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM8_Init();
  MX_USART3_UART_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

/* Start Timer Encoder */
  HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);
  HAL_TIM_Encoder_Start(&htim8, TIM_CHANNEL_ALL);

/* Start Timer Global Interupt */
  HAL_TIM_Base_Start_IT(&htim6);
  HAL_TIM_Base_Start_IT(&htim7);

/* Send and Receive Data */
  HAL_UART_Receive_DMA(&huart1, (uint8_t*) terimaJoystick, 9);
  HAL_UART_Receive_DMA(&huart2, (uint8_t*) receiveGyro, 7);
  HAL_UART_Receive_DMA(&huart3, (uint8_t*) receiveSlave, 31);
  HAL_UART_Transmit_DMA(&huart3, (uint8_t*) sendSlave, 31);



  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		if(stat_robot == 0)
		{
			outputSpeedOut[0] = axisJoystick[0] * 0.5;
			outputSpeedOut[1] = axisJoystick[1] * 0.5;
			outputSpeedOut[2] = axisJoystick[2] * 0.5;

			if(btnR1 == 0)
			{
//				test2++;
				outputSpeedOut[0] = axisJoystick[0];
				outputSpeedOut[1] = axisJoystick[1];
				outputSpeedOut[2] = axisJoystick[2];
			}
			if(btnR2 == 0)
			{
				outputSpeedOut[0] = axisJoystick[0]*2;
				outputSpeedOut[1] = axisJoystick[1]*2;
				outputSpeedOut[2] = axisJoystick[2]*2;
			}
		}

		if(stat_robot == 1)
		{
			test++;
//			if(btnX == 0)
//			{
//				motion_pure_pursuit(100, 100, 0, 100, 100, 100, outputSpeedOut);
				motion_jalan_x(100, 100, 0, 100, 100, 100, outputSpeedOut);
//			}
		}

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART3)
	{
		if ((receiveSlave[0] == 'i' && receiveSlave[1] == 't' && receiveSlave[2] == 's') == 0)
		{
			HAL_DMA_Abort(&hdma_usart3_rx);
			HAL_UART_Receive_DMA(&huart3, (uint8_t*) receiveSlave, 31);
		}
	}

	if (huart->Instance == USART2)
	{
		if ((receiveGyro[0] == 'i' && receiveGyro[1] == 't' && receiveGyro[2] == 's') == 0)
		{
//			test++;
			HAL_DMA_Abort(&hdma_usart2_rx);
			HAL_UART_Receive_DMA(&huart2, (uint8_t*) receiveGyro, 7);
		}
	}
	if (huart->Instance == USART1)
	{
		if ((terimaJoystick[0] == 'i' && terimaJoystick[1] == 't' && terimaJoystick[2] == 's') == 0)
		{
//			test++;
			HAL_DMA_Abort(&hdma_usart1_rx);
			HAL_UART_Receive_DMA(&huart1, (uint8_t*) terimaJoystick, 9);
		}
	}
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART3)
	{
		if ((receiveSlave[0] == 'i' && receiveSlave[1] == 't' && receiveSlave[2] == 's') == 1)
		{
//			test1++;
		}


	}
	if (huart->Instance == USART2)
	{
		if ((receiveGyro[0] == 'i' && receiveGyro[1] == 't' && receiveGyro[2] == 's') == 1)
		{
			test1++;
			if(statReceiveGyro == 0)
			{
				memcpy(&degree, receiveGyro+3,4);
				statReceiveGyro = 1;
			}
			else if(statReceiveGyro == 1)
			{
				memcpy(&degreeFix, receiveGyro+3,4);
				degreeFix = degreeFix - degree;
				filterDegree = -1*(0.005*filterDegree + 0.995*degreeFix);
			}
		}
	}

	if (huart->Instance == USART1)
	{
		if ((terimaJoystick[0] == 'i' && terimaJoystick[1] == 't' && terimaJoystick[2] == 's') == 1)
		{
			buttonsJoystick = (terimaJoystick[4] & 0xff) + ((terimaJoystick[3] & 0xff) << 8);
			btnL2		= (buttonsJoystick >> 0) & 0b1;
			btnR2		= (buttonsJoystick >> 1) & 0b1;
			btnL1 		= (buttonsJoystick >> 2) & 0b1;
			btnR1 		= (buttonsJoystick >> 3) & 0b1;
			btnSegitiga = (buttonsJoystick >> 4) & 0b1;
			btnBulat	= (buttonsJoystick >> 5) & 0b1;
			btnX 		= (buttonsJoystick >> 6) & 0b1;
			btnKotak 	= (buttonsJoystick >> 7) & 0b1;
			btnSelect 	= (buttonsJoystick >> 8) & 0b1;
			btnL3 		= (buttonsJoystick >> 9) & 0b1;
			btnR3 		= (buttonsJoystick >> 10) & 0b1;
			btnStart 	= (buttonsJoystick >> 11) & 0b1;
			btnAtas 	= (buttonsJoystick >> 12) & 0b1;
			btnKanan 	= (buttonsJoystick >> 13) & 0b1;
			btnBawah 	= (buttonsJoystick >> 14) & 0b1;
			btnKiri 	= (buttonsJoystick >> 15) & 0b1;

			axisJoystick[0] = terimaJoystick[7] - 127;
			axisJoystick[1] = -1*(terimaJoystick[8] - 127);
			axisJoystick[2] = terimaJoystick[5] -127;
			axisJoystick[3] = -1*(terimaJoystick[6] - 127);

			countStick++;

			btnMasuk = buttonsJoystick;
			if(btnMasuk != prevButtonsJoystick)
			{

				if(btnKotak == 0)
				{
					stat_robot = 0;
				}
				if(btnSegitiga == 0)
				{
					stat_robot = 1;
				}
			}

			prevButtonsJoystick = btnMasuk;


		}
	}
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	HAL_UART_Receive_DMA(&huart1, (uint8_t*) terimaJoystick, 9);
	HAL_UART_Receive_DMA(&huart2, (uint8_t*) receiveGyro, 7);
	HAL_UART_Receive_DMA(&huart3, (uint8_t*) receiveSlave, 31);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
