/*
 * motion_robot.c
 *
 *  Created on: Jan 10, 2023
 *      Author: adamm
 */
#include "math.h"
#include "motion_robot.h"
#include "stm32f4xx_hal.h"

extern float positionGlobal[3];
extern float globalSpeedOut[3];
extern float degree, degreeFix, filterDegree;
float varLGlobal, thetaGlobal;
extern uint8_t stat_robot; // 0 = Manual || 1 = Auto



void global_speed(float localSpeedOut[3], float globalSpeedOut[3])
{
	globalSpeedOut[0] = localSpeedOut[0]*cosf(filterDegree * 0.0174533) + localSpeedOut[1]*sinf(filterDegree * 0.0174533);
	globalSpeedOut[1] = -localSpeedOut[0]*sinf(filterDegree * 0.0174533) + localSpeedOut[1]*cosf(filterDegree * 0.0174533);
	globalSpeedOut[2] = localSpeedOut[2];
}

void motion_pure_pursuit(float target_x, float target_y, float target_z, float v_x, float v_y, float v_z, float globalSpeedOut_[3])
{
	float initPos[3];
	float varL, thetaTarget;
//	float p_[3], i_[3], d_[3];
//	float error[3], prev_error[3];
//	float kp_[3], ki_[3], kd_[3];

	for(int i=0;i<3;i++)
	{
		initPos[i] = positionGlobal[i];
	}

	while((positionGlobal[0] < target_x && positionGlobal[0] > initPos[0]) ||
			(positionGlobal[0] > target_x && positionGlobal[0] < initPos[0]) ||
			(positionGlobal[1] < target_y && positionGlobal[1] > initPos[1]) ||
			(positionGlobal[1] > target_y && positionGlobal[1] < initPos[1]))
	{
		varL = ((target_x - positionGlobal[0])*(target_x - positionGlobal[0]) + (target_y - positionGlobal[1]) * (target_y - positionGlobal[1]));

		varLGlobal = varL;

		thetaTarget = (2*positionGlobal[1])/(varL);
		thetaGlobal = thetaTarget;

		globalSpeedOut_[0] = target_x - positionGlobal[0];
		globalSpeedOut_[1] = target_y - positionGlobal[1];
		globalSpeedOut_[2] = thetaTarget - positionGlobal[2];

//		prev_error[2] = error[2];
//		error[2] = thetaTarget - positionGlobal[2];
//
//		p_[2] = kp_[2] * error[2];
//		i_[2] += ki_[2] * error[2];
//		d_[2] = kd_[2] * (error[2]-prev_error[2]);
//
//		globalSpeedOut[3] = p_[3] + i_[3] + d_[3];
//		globalSpeedOut_[0] = fminf(v_x,fmaxf(-v_x, globalSpeedOut_[0]));
//		globalSpeedOut_[1] = fminf(v_y,fmaxf(-v_y, globalSpeedOut_[1]));
//		globalSpeedOut_[2] = fminf(v_z,fmaxf(-v_z, globalSpeedOut_[2]));

		HAL_Delay(1);
	}
}
int count_motion;
float initPosGlobal[3];
float targetGlobal[3];

void motion_jalan_x(float target_x, float target_y, float target_w, float v_x, float v_y, float v_w, float globalSpeedOut_[3])
{
	float initPos[3];
	static float p_[3], i_[3], d_[3];
	static float error[3], prev_error[3];
	static float kp_[3], ki_[3], kd_[3];

	targetGlobal[0] = target_x;
	for(int i=0; i<3; i++)
	{
		initPos[i] = positionGlobal[i];
		initPosGlobal[i] = initPos[i];
	}


	while(((positionGlobal[0] < target_x && target_x > initPos[0]) ||
			(positionGlobal[0] > target_x && target_x < initPos[0])) && stat_robot != 0)
	{
		count_motion++;
		prev_error[0] = error[0];
		prev_error[1] = error[1];
		prev_error[2] = error[2];

		error[0] = target_x - positionGlobal[0];
		error[1] = target_y - positionGlobal[1];
		error[2] = target_w - positionGlobal[2];

		for(int i=0; i<2; i++)
		{
			p_[i]   = kp_[i] * error[i];
			i_[i]  += ki_[i] * error[i];
			d_[i]	= kd_[i] * (error[i] - prev_error[i]);

			globalSpeedOut[i] = p_[i] + i_[i] + d_[i];
		}

		globalSpeedOut_[0] = fminf(v_x,fmaxf(-v_x, globalSpeedOut_[0]));
		globalSpeedOut_[1] = fminf(v_y,fmaxf(-v_y, globalSpeedOut_[1]));
		globalSpeedOut_[2] = fminf(v_w,fmaxf(-v_w, globalSpeedOut_[2]));

		HAL_Delay(1);
	}
	stat_robot = 0;
}
